def backToNormalMode():
    global adjusting_tmo, mode
    music.play_tone(349, music.beat(BeatFraction.WHOLE))
    adjusting_tmo = 0
    mode = 0

def on_on_event():
    basic.show_number(hour)
control.on_event(8000, 8001, on_on_event)

def on_button_pressed_a():
    global adjusting_tmo, hour, minute
    if mode != 0:
        led.stop_animation()
        adjusting_tmo = 10
        music.play_tone(494, music.beat(BeatFraction.QUARTER))
        if mode == 1:
            hour += -1
            if hour <= -1:
                hour = 23
            control.raise_event(8000, 8001)
        elif mode == 2:
            minute += -1
            if minute <= -1:
                minute = 59
            control.raise_event(8000, 8002)
input.on_button_pressed(Button.A, on_button_pressed_a)

def on_on_event2():
    basic.show_number(minute)
control.on_event(8000, 8002, on_on_event2)

def on_button_pressed_b():
    global adjusting_tmo, hour, minute
    if mode != 0:
        led.stop_animation()
        adjusting_tmo = 10
        music.play_tone(523, music.beat(BeatFraction.QUARTER))
        if mode == 1:
            hour += 1
            if hour >= 24:
                hour = 0
            control.raise_event(8000, 8001)
        elif mode == 2:
            minute += 1
            if minute >= 60:
                minute = 0
            control.raise_event(8000, 8002)
input.on_button_pressed(Button.B, on_button_pressed_b)

def on_logo_touched():
    global mode, adjusting_tmo
    if mode == 0:
        mode = 1
        music.play_tone(262, music.beat(BeatFraction.EIGHTH))
        adjusting_tmo = 10
    elif mode == 1:
        mode = 2
        music.play_tone(294, music.beat(BeatFraction.EIGHTH))
        adjusting_tmo = 10
    elif mode >= 2:
        backToNormalMode()
input.on_logo_event(TouchButtonEvent.TOUCHED, on_logo_touched)

adjusting_tmo = 0
mode = 0
minute = 0
hour = 0
hour = 12
minute = 58
# 0 = nornal
# 1 = adj hour
# 2 = adj minute
mode = 0
adjusting_tmo = 0

def on_every_interval():
    global adjusting_tmo
    if adjusting_tmo > 0:
        adjusting_tmo += -1
        if adjusting_tmo <= 0:
            backToNormalMode()
loops.every_interval(1000, on_every_interval)

def on_every_interval2():
    global minute, hour
    minute += 1
    if minute >= 60:
        minute = 0
        hour += 1
        if hour >= 24:
            hour = 0
        music.play_melody("C5 A B G A F G E ", 120)
        music.play_melody("C5 G B A F A C5 B ", 120)
loops.every_interval(60000, on_every_interval2)

def on_forever():
    if mode == 0:
        if minute < 10:
            basic.show_string("" + convert_to_text(hour) + ":0" + convert_to_text(minute))
        else:
            basic.show_string("" + convert_to_text(hour) + ":" + convert_to_text(minute))
        basic.show_number(input.temperature())
        basic.show_leds("""
            # # . # #
                        # # # . .
                        . . # . .
                        . . # . .
                        . . . # #
        """)
        basic.clear_screen()
        basic.pause(100)
basic.forever(on_forever)

# auto backlight

def on_every_interval3():
    led.set_brightness(Math.constrain(pins.map(input.light_level(), 0, 255, 10, 200), 10, 200))
loops.every_interval(5000, on_every_interval3)
